// ignore_for_file: deprecated_member_use

import 'package:applore_project/helpers/Constants.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'HomePage.dart';

class LoginPage extends StatefulWidget {
  const LoginPage({Key? key}) : super(key: key);

  @override
  _LoginPageState createState() => _LoginPageState();
}

final _formKey = GlobalKey<FormState>();
bool isRegister = false;
TextEditingController emailController = TextEditingController();
TextEditingController passwordController = TextEditingController();

class _LoginPageState extends State<LoginPage> {
  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          centerTitle: true,
          title: const Text("Login Page"),
        ),
        body: SingleChildScrollView(
          child: Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(top: 60.0),
                child: Center(
                  child: SizedBox(
                      width: 200,
                      height: 150,
                      child: Image.asset('Assets/Images/flutter-logo.png')),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: TextFormField(
                    controller: emailController,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Email',
                        hintText: 'Enter valid email id as abc@gmail.com'),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Email';
                      }
                      return null;
                    }),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: 15.0, right: 15.0, top: 15, bottom: 0),
                child: TextFormField(
                    controller: passwordController,
                    obscureText: true,
                    decoration: const InputDecoration(
                        border: OutlineInputBorder(),
                        labelText: 'Password',
                        hintText: 'Enter secure password'),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Password';
                      }
                      return null;
                    }),
              ),
              const SizedBox(
                height: 30,
              ),
              Container(
                height: 50,
                width: 250,
                decoration: BoxDecoration(
                    color: Colors.blue,
                    borderRadius: BorderRadius.circular(20)),
                child: FlatButton(
                    onPressed: () {
                      if (_formKey.currentState!.validate()) {
                        isRegister ? userRegistration() : userLogin();
                      }
                    },
                    child: !isRegister
                        ? const Text(
                            'Login',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )
                        : const Text(
                            'Register',
                            style: TextStyle(color: Colors.white, fontSize: 25),
                          )),
              ),
              !isRegister
                  ? FlatButton(
                      onPressed: () {
                        setState(() {
                          isRegister = true;
                        });
                      },
                      child: const Text(
                        'New User, Create Account',
                        style: TextStyle(color: Colors.blue, fontSize: 15),
                      ),
                    )
                  : FlatButton(
                      onPressed: () {
                        setState(() {
                          isRegister = false;
                        });
                      },
                      child: const Text(
                        'Login To Existing Account',
                        style: TextStyle(color: Colors.blue, fontSize: 15),
                      ),
                    ),
            ],
          ),
        ),
      ),
    );
  }

  userRegistration() async {
    showDialog(
        context: context,
        builder: (context) => const Center(
              child: CircularProgressIndicator(),
            ));
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .createUserWithEmailAndPassword(
              email: emailController.text, password: passwordController.text);
      Navigator.pop(context);
      final User? user = userCredential.user;
      Constants.user = user!.uid;
      Constants.useremail = user.email!;
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const HomePage()));
    } on FirebaseAuthException catch (e) {
      Navigator.pop(context);
      if (e.code == 'weak-password') {
        Fluttertoast.showToast(msg: e.code);
        print('The password provided is too weak.');
      } else if (e.code == 'email-already-in-use') {
        Fluttertoast.showToast(msg: e.code);
        print('The account already exists for that email.');
      }
    }
  }

  userLogin() async {
    showDialog(
        context: context,
        builder: (context) => const Center(
              child: CircularProgressIndicator(),
            ));
    try {
      UserCredential userCredential = await FirebaseAuth.instance
          .signInWithEmailAndPassword(
              email: emailController.text, password: passwordController.text);
      Navigator.pop(context);
      final User? user = userCredential.user;
      Constants.user = user!.uid;
      Constants.useremail = user.email!;
      Navigator.pushReplacement(
          context, MaterialPageRoute(builder: (context) => const HomePage()));
    } on FirebaseAuthException catch (e) {
      Navigator.pop(context);
      if (e.code == 'user-not-found') {
        Fluttertoast.showToast(msg: e.code);
        print('No user found for that email.');
      } else if (e.code == 'wrong-password') {
        Fluttertoast.showToast(msg: e.code);
        print('Wrong password provided for that user.');
      }
    }
  }
}
