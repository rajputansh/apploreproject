// ignore_for_file: deprecated_member_use,

import 'dart:io';
import 'package:applore_project/helpers/Constants.dart';
import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:image_picker/image_picker.dart';
import 'package:flutter_native_image/flutter_native_image.dart';

class UploadData extends StatefulWidget {
  const UploadData({Key? key}) : super(key: key);

  @override
  _UploadDataState createState() => _UploadDataState();
}

class _UploadDataState extends State<UploadData> {
  final _formKey = GlobalKey<FormState>();
  FirebaseFirestore firestore = FirebaseFirestore.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('Products');
  File? finalImage;
  bool isLoading = false;
  double initSize = 0;
  double finalSize = 0;
  String imageUrl = "";
  TextEditingController nameController = TextEditingController();
  TextEditingController descriptionController = TextEditingController();
  TextEditingController priceController = TextEditingController();

  _imageUpload() async {
    final result = await ImagePicker().getImage(source: ImageSource.gallery);
    if (result == null) return;
    File image = File(result.path);
    setState(() {
      initSize = image.lengthSync() / 1024;
    });
    File compressedImage = await compressImage(imagePathToCompress: image);
    setState(() {
      finalSize = compressedImage.lengthSync() / 1024;
      finalImage = compressedImage;
    });
  }

  Future<File> compressImage(
      {required File imagePathToCompress,
      quality = 100,
      percentage = 10}) async {
    var path = await FlutterNativeImage.compressImage(
        imagePathToCompress.absolute.path,
        quality: 100,
        percentage: 10);
    return path;
  }

  @override
  Widget build(BuildContext context) {
    final fileName = finalImage != null ? finalImage?.path : 'No File Selected';
    return Scaffold(
      appBar: AppBar(
        centerTitle: true,
        title: const Text("Add Product"),
      ),
      body: SingleChildScrollView(
        child: Form(
          key: _formKey,
          child: Padding(
            padding: const EdgeInsets.only(top: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                FlatButton(
                  onPressed: () {
                    _imageUpload();
                  },
                  shape: const RoundedRectangleBorder(
                      side: BorderSide(color: Colors.grey, width: 2)),
                  child: Container(
                    decoration: const BoxDecoration(),
                    child: Padding(
                      padding: const EdgeInsets.all(20.0),
                      child: Column(
                        children: const [
                          Icon(
                            Icons.camera_alt,
                            color: Colors.grey,
                            size: 50,
                          ),
                          Text(
                            "Upload",
                            style: TextStyle(fontSize: 20, color: Colors.grey),
                          )
                        ],
                      ),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      fileName!,
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height * 0.01,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      "Initial Size" + initSize.toString() + " KB",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height * 0.01,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(8.0),
                  child: Center(
                    child: Text(
                      "Final Size" + finalSize.toString() + " KB",
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          fontSize: MediaQuery.of(context).size.height * 0.01,
                          fontWeight: FontWeight.w500),
                    ),
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextFormField(
                    controller: nameController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Product Name',
                      hintText: 'Product Name',
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Product Name';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextFormField(
                    controller: descriptionController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Description',
                      hintText: 'Description',
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Product Description';
                      }
                      return null;
                    },
                  ),
                ),
                Padding(
                  padding: const EdgeInsets.all(15),
                  child: TextFormField(
                    keyboardType: TextInputType.number,
                    controller: priceController,
                    decoration: const InputDecoration(
                      border: OutlineInputBorder(),
                      labelText: 'Price',
                      hintText: 'Price',
                    ),
                    validator: (value) {
                      if (value == null || value.isEmpty) {
                        return 'Please Enter Product Price';
                      }
                      return null;
                    },
                  ),
                ),
                Container(
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(20),
                  ),
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height * 0.07,
                  margin: const EdgeInsets.all(25),
                  child: FlatButton(
                    child: const Text(
                      'Submit',
                      style: TextStyle(fontSize: 20.0),
                    ),
                    color: const Color(0xff1D2677),
                    textColor: Colors.white,
                    onPressed: () async {
                      if (_formKey.currentState!.validate() &&
                          finalImage != null) {
                        showDialog(
                            context: context,
                            builder: (context) => const Center(
                                  child: CircularProgressIndicator(),
                                ));
                        await uploadImage();
                        if (imageUrl == "" || imageUrl == null) {
                          Fluttertoast.showToast(msg: "Please Select a Image");
                        } else {
                          await FirebaseFirestore.instance
                              .collection(Constants.user)
                              .add({
                            "Name": nameController.text,
                            "Description": descriptionController.text,
                            "ImageUrl": imageUrl,
                            "Price": priceController.text
                          }).then((value) {
                            Navigator.pop(context);
                            Fluttertoast.showToast(msg: "Success");
                            nameController.text = "";
                            priceController.text = "";
                            descriptionController.text = "";
                            finalImage = null;
                            initSize = 0;
                            finalSize = 0;
                          });
                        }
                      }
                      setState(() {
                        isLoading = true;
                      });
                    },
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  uploadImage() async {
    final _firebaseStorage = FirebaseStorage.instance;
    var file = finalImage;
    var snapshot =
        await _firebaseStorage.ref().child(nameController.text).putFile(file!);
    var downloadUrl = await snapshot.ref.getDownloadURL();
    setState(() {
      imageUrl = downloadUrl;
    });
  }
}
