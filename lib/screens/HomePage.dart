import 'package:applore_project/helpers/Constants.dart';
import 'package:applore_project/screens/UploadData.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:pull_to_refresh/pull_to_refresh.dart';

import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  _HomePageState createState() => _HomePageState();
}

FirebaseFirestore firestore = FirebaseFirestore.instance;

class _HomePageState extends State<HomePage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: const Text("Products"),
          actions: [
            Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => const UploadData()));
                  },
                  child: const Icon(
                    Icons.add,
                    size: 26.0,
                  ),
                )),
          ],
          centerTitle: true,
          automaticallyImplyLeading: false,
        ),
        body: loading
            ? const Center(
                child: Text("Loading Products..."),
              )
            : Container(
                child: productList.isEmpty
                    ? const Center(child: Text("No Data"))
                    : Scrollbar(
                        child: SmartRefresher(
                          onLoading: _onLoading,
                          onRefresh: _onRefresh,
                          controller: _refreshController,
                          child: ListView.builder(
                              controller: _scrollController,
                              itemCount: productList.length,
                              itemBuilder: (BuildContext context, int index) {
                                return ListTile(
                                  minLeadingWidth: 30,
                                  leading: ConstrainedBox(
                                    constraints: const BoxConstraints(
                                      minWidth: 44,
                                      minHeight: 44,
                                      maxWidth: 64,
                                      maxHeight: 64,
                                    ),
                                    child: Image.network((productList[index]
                                        .data() as dynamic)["ImageUrl"]),
                                  ),
                                  title: Padding(
                                    padding: const EdgeInsets.all(8.0),
                                    child: Text(
                                      (productList[index].data()
                                              as dynamic)["Name"]
                                          .toString()
                                          .toUpperCase(),
                                      style: const TextStyle(
                                          fontSize: 30,
                                          // color: Colors.bl,
                                          fontWeight: FontWeight.w500),
                                    ),
                                  ),
                                  subtitle: Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    children: [
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          (productList[index].data()
                                              as dynamic)["Description"],
                                          style: const TextStyle(
                                              fontSize: 15,
                                              color: Colors.grey,
                                              fontStyle: FontStyle.italic),
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.all(8.0),
                                        child: Text(
                                          "Rs." +
                                              (productList[index].data()
                                                  as dynamic)["Price"],
                                          style: const TextStyle(
                                            fontSize: 15,
                                            color: Colors.green,
                                          ),
                                        ),
                                      ),
                                    ],
                                  ),
                                );
                              }),
                        ),
                      )));
  }

  final RefreshController _refreshController =
      RefreshController(initialRefresh: false);
  final ScrollController _scrollController = ScrollController();
  List<DocumentSnapshot> productList = [];
  int pageSize = 7;
  bool loading = true;
  bool nextProduct = false;
  bool nextProductExist = true;
  late DocumentSnapshot _lastDoc;
  getProductsList() async {
    Query q =
        firestore.collection(Constants.user).orderBy("Name").limit(pageSize);
    QuerySnapshot querySnapshot = await q.get();
    setState(() {
      loading = true;
    });
    productList = querySnapshot.docs;
    _lastDoc = querySnapshot.docs[querySnapshot.docs.length - 1];
    setState(() {
      loading = false;
    });
  }

  getNextProductsList() async {
    if (nextProductExist == false) {
      return;
    }

    if (nextProduct == true) {
      return;
    }
    setState(() {
      nextProduct = true;
    });
    Query q = firestore
        .collection(Constants.user)
        .orderBy("Name")
        .startAfter([(_lastDoc.data() as dynamic)["Name"]]).limit(pageSize);
    QuerySnapshot querySnapshot = await q.get();
    if (querySnapshot.docs.length < pageSize) {
      setState(() {
        nextProductExist = false;
      });
    }

    _lastDoc = querySnapshot.docs[querySnapshot.docs.length - 1];

    setState(() {
      productList.addAll(querySnapshot.docs);
    });
    setState(() {
      nextProduct = false;
    });
  }

  void _onRefresh() async {
    getProductsList();
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.refreshCompleted();
  }

  void _onLoading() async {
    getProductsList();
    await Future.delayed(Duration(milliseconds: 1000));
    _refreshController.loadComplete();
  }

  @override
  void initState() {
    getProductsList();
    super.initState();

    _scrollController.addListener(() {
      double maxScroll = _scrollController.position.maxScrollExtent;
      double currScroll = _scrollController.position.pixels;
      double diff = MediaQuery.of(context).size.height * 0.25;

      if (maxScroll - currScroll <= diff) {
        getNextProductsList();
      }
    });
  }
}
